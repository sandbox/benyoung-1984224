<?php
/**
 * @file
 * Taxonomy descendants condition class for Context.
 */

/**
 * Expose children of node taxonomy terms as a context condition.
 */
class taxonomy_descendants_condition extends context_condition_node_taxonomy {
  function options_form($context) {
    return array();
  }

  function execute($node, $op) {
    if (!$this->condition_used()) {
      return;
    }

    $fields = field_info_fields();
    $instance_fields = field_info_instances('node', $node->type);
    $check_fields = array();

    foreach ($instance_fields as $key => $field_info) {
      if ($fields[$key]['type'] === 'taxonomy_term_reference') {
        $check_fields[] = $key;
      }
    }

    if (empty($check_fields)) {
      return;
    }

    foreach ($check_fields as $field) {
      if ($terms = field_get_items('node', $node, $field)) {
        foreach ($terms as $term) {
          $parents = taxonomy_get_parents_all($term['tid']);
          array_shift($parents);

          foreach ($parents as $parent) {
            foreach ($this->get_contexts($parent->tid) as $context) {
              $this->condition_met($context, $parent->tid);
            }
          }
        }
      }
    }
  }
}
