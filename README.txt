Condition plugin for Context (http://drupal.org/project/context) that activates
when viewing a node referencing descendants of the specified taxonomy terms.

This plugin can be contrasted with Context's built-in Taxonomy condition with
the following example:

Term A
- Term B
-- Term C

In this example, with Context's Taxonomy condition, you can activate a context
when any terms beneath Term A are referenced by the node in view by manually
selecting Term B and Term C. If another term is added beneath Term A, it must
be added to the condition rule by hand.

Instead, you can use the Taxonomy Descendants condition this module provides
and select Term A. The condition will be activated when a node references any
descendants of Term A and no further configuration is required.
